﻿describe("pow", function() {

  it("при возведении 2 в 3ю степень результат 8", function() {
    assert.equal(pow(2, 3), 8);
  });

  it("при возведении 3 в 4ю степень равен 81", function() {
    assert.equal(pow(3, 4), 81);
  });

});

describe("SendMess.ShowNumUsers", function() {
    it(" ShowNumUsers = 2", function () {
        document.getElementById('bcSendMess_ListUsers').value =
            "1 - test test \n 2 - test test";

        bc.SendMess.ShowNumUsers();
        var eNumUserInnerText = document.getElementById("bcSendMess.NumUsers").innerText;

        assert.equal(eNumUserInnerText, "2 человек");
    });
});

describe("SendMess.NewParseeUser", function () {
    it(" NewParseeUser = 2", function () {
        document.getElementById('bcSendMess_ListUsers').value =
            "1 - test test \n 2 - test test";

        bc.SendMess.ShowNumUsers();
        var eNumUserInnerText = document.getElementById("bcSendMess.NumUsers").innerText;

        assert.equal(eNumUserInnerText, "2 человек");
    });
});