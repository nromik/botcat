﻿bc.Invite = {
    debug: false,
    isStop: true,
    Locale: null,
    UserBlocs: {},
    urlSearchUser: null,
    blockInvate: [], // загружаем из ресурса 

    currentUser: 0, // текущий пользователь
    invitedUsers: 0, // приглашено успешно
    sendInvitedUsers: 0, // выслано приглашений

    Timer: {
        ElementFocus: null,
        ElementClick: null,
        Captcha: null,
        Invite: null,
        SearchUser: null
    },
    UserBlocs_oldLength: 0
}; // bc.Invate

bc.Invite.BeginView = function (type) {
    var beginNumber = "";
    var startWithUser = null;
    switch (type) {
        case 2:
            beginNumber = bc.Invite.currentUser;
            break;
        case 3:
            startWithUser = bc.Invite.LoadUser_LocalStorage();
            break;
    }
    bc.ge("bcInvite_StartWithNum").value = beginNumber;
    bc.ge("bcInvite_StartWithUser").value = startWithUser;
}

//bc.Invite.BeginFirstUser = function () {
//    //Онулять позицию текущего пользователя 
//    bc.Invite.Stop();
//    bc.Invite.currentUser = 0;
//    bc.Invite.invitedUsers = 0;
//    bc.Invite.sendInvitedUsers = 0;
//
//    document.getElementById("bcInvate_StartWithUser").value = "";
//
//    var groupId = bc.Invite.getGroupId();
//    if (!groupId) return;
//    //localStorage.removeItem("bc_" + groupId);
//
//};
//
//bc.Invite.BeginLastUser = function () {
//    //Онулять позицию текущего пользователя 
//    bc.Invite.Stop();
//    bc.Invite.currentUser = 0;
//    bc.Invite.invitedUsers = 0;
//    bc.Invite.sendInvitedUsers = 0;
//
//    var urlNextUser = bc.Invite.LocalStorageGetNextUser();
//
//    document.getElementById("bcInvate_StartWithUser").value = urlNextUser;
//
//};

bc.Invite.LoadUser_LocalStorage = function () {
    var groupId = bc.Invite.GetGroupId();
    if (!groupId) return;

    var userSave = localStorage.getItem("bc_" + groupId);
    return userSave ? userSave : null;
};

bc.Invite.SaveUser_LocalStorage = function () {
    var groupId = bc.Invite.GetGroupId();
    if (!groupId) return;

    var currentUser = jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0].href;
    localStorage.setItem("bc_" + groupId, currentUser);
};

bc.Invite.GetGroupId = function () {
    var groupId = window.location.search.match(/group_id=\d+/)[0];
    if (!groupId) {
        bc.Log.Add("Перейдите на страницу для приглашения");
    }
    return groupId;
};

bc.Invite.CheckPage = function() {
    if (location.pathname !== "/friends") {
        bc.Log.Add("перейдите на страныцу для приглашения");
        bc.ge("bcInvite_Stop").click();
        return false;
        /*todo: Доделать проверку*/
    }
    return true;
}

bc.Invite.Search = function() {
    var currUser = null;
    var searhUser = "", i = 0, interval = 0;
    if (!bc.Invite.CheckPage()) return;

    var typeSearch = jQuery("input[name='bcInvate_Begin']:checked").val();
    var beginNumber = bc.ge("bcInvite_StartWithNum").value;

    bc.Invite.UserBlocs = jQuery(".friends_user_row");

    switch (typeSearch) {
        case "0":
            bc.ge("bcInvite_currentUser").innerText = bc.Invite.currentUser;
            currUser = jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0];
            currUser.focus();
            currUser.style.color = "#FF00FF";

            bc.ge("bcInvite_Stop").click();
            return;
        case "1":
            bc.ge("bcInvite_currentUser").innerText = 0;
            var firstUser = jQuery(bc.Invite.UserBlocs[0]).find(".friends_field_title a")[0];
            firstUser.focus();
            firstUser.style.color = "#FF00FF";
            
            bc.ge("bcInvite_Stop").click();
            return;

        case "2":

            if (beginNumber < bc.Invite.UserBlocs.length) {
                bc.Invite.currentUser = beginNumber;
                currUser = jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0];
                currUser.focus();
                currUser.style.color = "#FF00FF";

                bc.ge("bcInvite_currentUser").innerText = bc.Invite.currentUser;
                bc.ge("bcInvite_Stop").click();
                return;
            }

            bc.ge("bcInvite_currentUser").innerText = bc.Invite.UserBlocs.length;

            // не загрузились новые блоки
            if (bc.Invite.UserBlocs_oldLength === bc.Invite.UserBlocs.length) {
                bc.Log.Add("Если это не конец списка то увеличьте Interval");
                bc.ge("bcInvite_Stop").click();
                return;
            }
            bc.Invite.UserBlocs_oldLength = bc.Invite.UserBlocs.length;


            Friends.showMore(-1);
            currUser = jQuery(bc.Invite.UserBlocs[bc.Invite.UserBlocs.length - 1]).find(".friends_field_title a")[0];
            currUser.focus();
            currUser.style.color = "#FF00FF";

            if (beginNumber > bc.Invite.UserBlocs.length) {
                interval = bc.Lib.GetInterval("bcInvite_Interval", 2, 3) * 1000;
                bc.Invite.Timer.SearchUser = setTimeout(bc.Invite.Search, interval / 4);
                return;
            }
            
            return;
        case "3":
            searhUser = bc.Invite.LoadUser_LocalStorage();
            // нету в хранилище 
            if (!searhUser) {
                bc.Invite.currentUser = 0;
                currUser = jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0];
                currUser.focus();
                currUser.style.color = "#FF00FF";

                bc.ge("bcInvite_currentUser").innerText = bc.Invite.currentUser;
                bc.ge("bcInvite_Stop").click();
                return;
            }

            for (i = bc.Invite.UserBlocs_oldLength; i < bc.Invite.UserBlocs.length; i++) {

                if (jQuery(bc.Invite.UserBlocs[i]).find(".friends_field_title a")[0].href === searhUser) {
                    bc.Invite.currentUser = i;
                    currUser = jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0];
                    currUser.focus();
                    currUser.style.color = "#FF00FF";

                    bc.ge("bcInvite_currentUser").innerText = bc.Invite.currentUser;
                    bc.ge("bcInvite_Stop").click();
                    return;
                }

            }


            bc.ge("bcInvite_currentUser").innerText = bc.Invite.UserBlocs.length;

            // не загрузились новые блоки
            if (bc.Invite.UserBlocs_oldLength === bc.Invite.UserBlocs.length) {
                bc.Log.Add("Если это не конец списка то увеличьте Interval");
                bc.ge("bcInvite_Stop").click();
                return;
            }
            bc.Invite.UserBlocs_oldLength = bc.Invite.UserBlocs.length;


            Friends.showMore(-1);
            currUser = jQuery(bc.Invite.UserBlocs[bc.Invite.UserBlocs.length - 1]).find(".friends_field_title a")[0];
            currUser.focus();
            currUser.style.color = "#FF00FF";

            interval = bc.Lib.GetInterval("bcInvite_Interval", 2, 3) * 1000;
            bc.Invite.Timer.SearchUser = setTimeout(bc.Invite.Search, interval / 4);
            return;

        case "4":
            searhUser = bc.ge("bcInvite_StartWithUser").value;
            for (i = bc.Invite.UserBlocs_oldLength; i < bc.Invite.UserBlocs.length; i++) {

                if (jQuery(bc.Invite.UserBlocs[i]).find(".friends_field_title a")[0].href === searhUser) {
                    bc.Invite.currentUser = i;
                    currUser = jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0];
                    currUser.focus();
                    currUser.style.color = "#FF00FF";

                    bc.ge("bcInvite_currentUser").innerText = bc.Invite.currentUser;
                    bc.ge("bcInvite_Stop").click();
                    return;
                }
            }

            bc.ge("bcInvite_currentUser").innerText = bc.Invite.UserBlocs.length;

            // не загрузились новые блоки
            if (bc.Invite.UserBlocs_oldLength === bc.Invite.UserBlocs.length) {
                bc.Log.Add("Если это не конец списка то увеличьте Interval");
                bc.ge("bcInvite_Stop").click();
                return;
            }
            bc.Invite.UserBlocs_oldLength = bc.Invite.UserBlocs.length;


            Friends.showMore(-1);
            currUser = jQuery(bc.Invite.UserBlocs[bc.Invite.UserBlocs.length - 1]).find(".friends_field_title a")[0];
            currUser.focus();
            currUser.style.color = "#FF00FF";

           
            interval = bc.Lib.GetInterval("bcInvite_Interval", 2, 3) * 1000;
            bc.Invite.Timer.SearchUser = setTimeout(bc.Invite.Search, interval / 4);
            return;
    }
}





bc.Invite.Start = function () {
    bc.Invite.isStop = false;

    if (!bc.Invite.CheckPage()) return;

    
    
    bc.Invite.UserBlocs = jQuery(".friends_user_row");

    var typeStart = jQuery("input[name='bcInvate_Begin']:checked").val();

    switch (typeStart) {
        case "0":
            break;
        case "1":
            bc.Invite.currentUser = 0;
            break;
        case "2":
            var beginNumber = bc.ge("bcInvite_StartWithNum").value;
            if (bc.Invite.UserBlocs > beginNumber) {
                bc.Invite.currentUser = beginNumber;
                break; 
            }
            bc.Log.Add("Сначала нужно найти пользователя ");
            bc.ge("bcInvite_Stop").click();
            return;
            
        case "3":
            try {
                if (jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0].href === bc.Invite.LoadUser_LocalStorage()) {
                    break;
                }

            } catch (e) {
                bc.Log.Add("Start Error = " + e.toString());
            } 
            bc.Log.Add("Сначала нужно найти пользователя ");
            bc.ge("bcInvite_Stop").click();
            return;
        case "4":
            try {
                if (jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0].href === bc.ge("bcInvite_StartWithUser").value) {
                    break;
                }
            } catch (e) {
                bc.Log.Add("Start Error = " + e.toString());
            }
            bc.ge("bcInvite_Stop").click();
            return;
    }
    var currUser = jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0];
    currUser.focus();
    currUser.style.color = "#FF00FF";

    try {
        jQuery("input[name='bcInvate_Begin']")[0].click();
    } catch (e) { } 

    bc.Invite.Invite();
};

bc.Invite.Stop = function () {
    bc.Invite.UserBlocs_oldLength = 0; 
    bc.Invite.isStop = true;
    window.clearTimeout(bc.Invite.Timer.ElementFocus);
    window.clearTimeout(bc.Invite.Timer.ElementClick);
    window.clearTimeout(bc.Invite.Timer.Captcha);
    window.clearTimeout(bc.Invite.Timer.Invite);
    window.clearTimeout(bc.Invite.Timer.SearchUser);

    bc.ge("bcInvite_Stop").click();

};



//bc.Invite.SearchUser = function () {
//    if (bc.Invite.isStop) return;

//    var currentUser = bc.Invite.currentUser;

//    // выводим статистику 
//    jQuery("#bcInvate_currentUser").text(currentUser);
//    bc.Invite.ProgressBar();

//    if (bc.Invite.CheckEndAndGetNewUserBlocs()) return;
//    var linkCurrUser = jQuery(bc.Invite.UserBlocs[currentUser]).find(".friends_field_title a")[0]

//    linkCurrUser.focus();

//    if (currentUser > 0) {
//        linkCurrUser.style.color = "#ee0";
//    }

//    //console.log(bc.Invite.UserBlocs[currentUser].children[1].children[0].children[0].href + " " + bc.Invite.urlSearchUser);
//    if (linkCurrUser.href === bc.Invite.urlSearchUser) { // найден пользователь 

//        jQuery("#bcInvate_StartWithUser")[0].value = "";
//        bc.Invite.Invite();
//        return;
//    }

//    bc.Invite.currentUser = currentUser + 1;
//    bc.Invite.Timer.SearchUser = setTimeout(bc.Invite.SearchUser, 100, bc.Invite.urlSearchUser);
//};




bc.Invite.Invite = function () {
    if (bc.Invite.isStop) return;

    if (bc.Invite.CheckEndAndGetNewUserBlocs()) return;

    // Check invite previous user
    bc.Invite.CheckInvitePrevUser();

    var curUser = bc.Invite.currentUser;

    bc.Invite.ShowStatistic();

    console.log("Invating user = " + curUser + " - " +
        jQuery(bc.Invite.UserBlocs[curUser]).find(".friends_field_title a")[0].innerText);

    // Mark the current user in the red color
    jQuery(bc.Invite.UserBlocs[curUser]).find(".friends_field_title a")[0].style.color = "red";

    if (curUser > 0)
        jQuery(bc.Invite.UserBlocs[curUser - 1]).find(".friends_field_title a")[0].style.color = "#aaaa00";

    
    
   

    //сохраняем следующего пользователя в хран
    bc.Invite.LoadUser_LocalStorage();


    var kInteval = bc.Lib.GetInterval("bcInvite_Interval", 2, 4) / 2.5;
    //var stausInvate = bc.Invite.UserBlocs[curUser].children[2].children[0].innerText;/*stausInvate === "Выслать приглашение" || stausInvate === "Пригласить в группу"*/

    var userId = bc.Invite.UserBlocs[curUser].id.toString().match(/\d+/)[0];
    //если пользователь есть в листе заблокированых
    if (bc.blockInvate[userId]) {
        jQuery(jQuery(bc.Invite.UserBlocs[curUser]).find("button"))[0].style.backgroundColor = "#994";
        bc.Invite.Timer.ElementFocus = setTimeout(bc.Invite.ElementFocus, 10 * kInteval);
        bc.Invite.Timer.Captcha = setTimeout(bc.Invite.Captcha, 100 * kInteval);
    }
    // проверяем статус кнопки "Отменить приглашение"
    else if (jQuery(jQuery(bc.Invite.UserBlocs[curUser]).find("button.secondary , button.button_disabled"))[0]) {
        bc.Invite.Timer.ElementFocus = setTimeout(bc.Invite.ElementFocus, 10 * kInteval);
        bc.Invite.Timer.Captcha = setTimeout(bc.Invite.Captcha, 100 * kInteval);

    } else {
        bc.Invite.Timer.ElementFocus = setTimeout(bc.Invite.ElementFocus, 500 * kInteval);
        bc.Invite.Timer.ElementClick = setTimeout(bc.Invite.ElementClick, 1500 * kInteval);
        bc.Invite.Timer.Captcha = setTimeout(bc.Invite.Captcha, 2500 * kInteval);; // создан отдельный поток по котором пойдет программа
    }
};

bc.Invite.CheckEndAndGetNewUserBlocs = function () {

    var currentUser = bc.Invite.currentUser;

    if (!(bc.Invite.UserBlocs.length > currentUser)) {
        bc.Invite.UserBlocs = jQuery(".friends_user_row");
    }

    if (!(bc.Invite.UserBlocs.length > currentUser)) { // если новых елементов не найдено стоп
        bc.Invite.Stop();
        console.log("Stop - (currentUser =" + currentUser + ") >= (bc.Invite.UserBlocs.length = " + bc.Invite.UserBlocs.length + ")");
        return true; // STOP
    }
    return false;
};

bc.Invite.CheckInvitePrevUser = function () {
    
    jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0].style.color = "#00a";

    if (bc.Invite.currentUser > 0) {

        bc.Invite.sendInvitedUsers++; // +1 выслано приглашений

        var previousUser = bc.Invite.UserBlocs[bc.Invite.currentUser - 1];

       
        var res = jQuery(jQuery(previousUser).find(".msg_text")[0]).text();
        if (res) { // найдено ошибка 

            
            
            var ifMaxInvate = res.match(/\d+/);
            if (ifMaxInvate && ifMaxInvate[0] === "40") {
                bc.Log.Add("vr.Error - " + res);
                bc.Invite.Stop();
                return;
            }

            var userIdOld = previousUser.id.toString().match(/\d+/)[0];
            var userNameOld = jQuery(previousUser).find(".friends_field_title a")[0].innerText;

            bc.ge("bcInvite_ListUsers").value += bc.SendMess.UserGetAsLine(userIdOld, userNameOld, "") + " -\n";
            jQuery(previousUser).find(".friends_field_title a")[0].style.color = "#a00";


            var idScript = "bc_blockInvate_add";
            if (bc.ge(idScript)) {
                document.head.removeChild(bc.ge(idScript));
            }
            var script = document.createElement("script");
            script.src = bc.hostApi + "blockInvate_add.php?" +
                "id=" + userIdOld +
                "&name=" + bc.Lib.Translit(userNameOld) +
                "&who_add=" + bc.vkUser +
                "&session="+bc.session;
            script.id = idScript;
            document.head.appendChild(script);

            bc.ge("bcInvite_countBlockUser").innerText = Object.keys(bc.blockInvate).length;

           
        } else {
            bc.Invite.invitedUsers++;
            jQuery(previousUser).find(".friends_field_title a")[0].style.color = "#0a0";

        }
    }
};

bc.Invite.ElementFocus = function () {
    if (bc.Invite.isStop) return;
    //фокусируемся на имени 
    jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find(".friends_field_title a")[0].focus();
};

bc.Invite.ElementClick = function () {
    if (bc.Invite.isStop) return;
    jQuery(bc.Invite.UserBlocs[bc.Invite.currentUser]).find("button")[0].click();
};

bc.Invite.Captcha = function () {
    if (bc.Invite.isStop) return;
    var captcha = jQuery(".box_layout");
    if (captcha.length > 0) { // страно но она при отсутствии равна []
        console.log("captcha");
        setTimeout(bc.Invite.Captcha, 5000);

        if (bc.ge("bcInvite_isSound").checked) {
            bc.Lib.audio.volume = bc.Lib.audioVolume;
            bc.Lib.audio.play();
        }
        return;
    }
    
    bc.Invite.currentUser++;
    bc.Invite.Timer.Invite = setTimeout(bc.Invite.Invite, 100);
};

bc.Invite.LoadBlockList = function() {
    var script = document.createElement("script");
    script.src = bc.hostApi + "blockInvate_get.php";
    document.head.appendChild(script);

    script.onload = function() {
        bc.ge("bcInvite_countBlockUser").innerText = Object.keys(bc.blockInvate).length;
    }
    script.onerror = function () {
        bc.ge("bcInvite_countBlockUser").innerText = "Error load file";
    }
}

bc.Invite.ShowStatistic = function() {
    bc.ge("bcInvite_invitedUsers").innerText = bc.Invite.invitedUsers; // приглашено успешно
    bc.ge("bcInvite_sendInvitedUsers").innerText = bc.Invite.sendInvitedUsers; // выслано приглашений
    bc.ge("bcInvite_currentUser").innerText = bc.Invite.currentUser;
    bc.Invite.ProgressBar();
}

bc.Invite.ProgressBar = function () {

    var quantityFriendString;
    if (jQuery("._friends_header:visible.friends_tabs").length > 0) {
        quantityFriendString = jQuery("._friends_header:visible.friends_tabs").find(".ui_tab_sel span").text();
        //console.log("Мои Друзья (sel) = " + quantityFriendString + " <- bc.Invite.ProgressBar");
    }
    else if (jQuery("._friends_header:visible.page_block_header").length > 0) {
        quantityFriendString = jQuery("._friends_header:visible.page_block_header").find("#friends_list_count").text();
        //console.log("Списки друзей (sel) = " + quantityFriendString + " <- bc.Invite.ProgressBar");
    } else {
        bc.Log.Add("Не могу с этой страницы найти пользователей. <- bc.Invite.ProgressBar");
        return;
    }
    var quantityFriend = quantityFriendString.replace(/[ ]/g, "") * 1;
                      
    var progressBar = bc.ge("bcInviteProgress");
    progressBar.max = quantityFriend;
    progressBar.value = bc.Invite.currentUser;
};

console.log("Load Invite");