﻿"use strict";
bc.Log = {
    iLog: 0,

    Add: function (message) {
        bc.Log.iLog++;

        var list = document.getElementById("bcFooter_ErrorList_ol");
        var item = document.createElement("li");

        item.innerHTML = message + '<i class="fa fa-times" onclick="bc.Log.ClearCurront(' + (bc.Log.iLog) + ')"></i>';
        item.id = 'bcLogNum_' + bc.Log.iLog;
        if (list.firstChild) {
            // вставка элемента перед указанным элементом.
            list.insertBefore(item, list.firstChild);
        } else {
            list.appendChild(item);
        }

    },

    ClearAll: function () {
        var list = document.getElementById("bcFooter_ErrorList_ol");
        while (list.children.length > 0) {
            list.removeChild(list.children[0]);
        }

    },

    ClearCurront: function (iLog) {
        var list = document.getElementById("bcFooter_ErrorList_ol");
        for (var i in list.children) {
            if (list.children[i].id === "bcLogNum_" + iLog)
                list.removeChild(list.children[i]);
        }
    }


}; //bc.Log

bc.Log.Empty = function () { };
console.log("Load Log");