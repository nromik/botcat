﻿"use strict";
bc.SendMess = {
    ListUsers: "", //Список пользователей [id \t- FirstName LastName]\n*  ->  For User And localStorage
    MessageText: "", //Текст сообщения -> "{ Hello | Hi } text { Goodbye | Bye | So long } { . | ! | !!! | :) }"
    MessageMedia: "", //Силки на медиа файлы  -> { video | image } 

    Users: null, //obj GetUsers(bc.ListUsers)     -> *[1-n].id .FirstName .LastName
    Message: null, //obj GetMessage(bc.MessageText) -> *[] or *[][] - рваний массив 
    Media: null, //obj GetMedia(bc.MessageMedia)  -> *[type] [id] type : { video | image } 

    timer: {
        SelectDialog: null,
        CloseDialog: null,
        WriteMessage: null,
        AddMedia: null,
        SendMessage: null,
        Send: null,
        RemoveUsers: null,
        UpDateListUser: null
    }, // таймеры

    isSendStep: 0,

    ShowNumUsers: function () {
        //"use strict";
        var listUser = document.getElementById('bcSendMess_ListUsers').value,
            numUsers = 0;
        if (listUser || listUser !== "") {
            numUsers = listUser.split("\n").length;
        }
        document.getElementById("bcSendMess.NumUsers").innerText = numUsers + " человек";
    },

    NewParseeUser: function () {
        // попитка спарсить число из дива  -- "У Вас 1 500 друзей "
        var quantityFriendString = "",
            quantityFriend = 0,
            i,
            id;


        if (jQuery("._friends_header:visible.friends_tabs").length > 0) {
            quantityFriendString = jQuery("._friends_header:visible.friends_tabs").find(".ui_tab_sel span").text();
            console.log("Мои Друзья (sel) = " + quantityFriendString + " <- bc.SendMess.NewParseeUser");
        }
        else if (jQuery("._friends_header:visible.page_block_header").length > 0) {
            quantityFriendString = jQuery("._friends_header:visible.page_block_header").find("#friends_list_count").text();
            console.log("Списки друзей (sel) = " + quantityFriendString + " <- bc.SendMess.NewParseeUser");
        } else {
            bc.Log.Add("Не могу с этой страницы считать пользователей.");
            return;
        }

        quantityFriend = quantityFriendString.replace(/[ ]/g, "") * 1;


        for (i = -15; i < quantityFriend; i += 15) { // розворачиваем список друзей , подгружается по 15 записей на вызов метода
            Friends.showMore(-1);
        }

        var friendsRow = jQuery(".friends_user_row"); // выбераем все дивы которые содержат друзей

        var listUsers = [friendsRow.lengh];

        for (i = 0; i < friendsRow.length; i += 1) { // формируем масив который содержыт список друзей id - имя фамилия 
            id = friendsRow[i].id.match(/\d+/);
            listUsers[i] = bc.SendMess.UserGetAsLine(id, jQuery(friendsRow[i]).find(".friends_field_title").text(), ""); // находим имя 
        }

        bc.SendMess.ListUsers = listUsers.join("\n"); // сохроняем в внешний обект
        jQuery("#bcSendMess_ListUsers").text(bc.SendMess.ListUsers); // показываем пользователь

        bc.SendMess.ShowNumUsers();
    },

    SeveLocalStorage: function () {
        var listUsers,
            messageText,
            messageMedia;

        listUsers = document.getElementById("bcSendMess_ListUsers").value;
        messageText = document.getElementById("bcSendMess_MessText").value;
        messageMedia = document.getElementById("bcSendMess_MessMedia").value;

        // Перенос из textArea in Bot
        bc.SendMess.ListUsers = listUsers;
        bc.SendMess.MessageText = messageText;
        bc.SendMess.MessageMedia = messageMedia;

        localStorage.setItem("bcSendMess_ListUsers", listUsers);
        localStorage.setItem("bcSendMess_MessText", messageText);
        localStorage.setItem("bcSendMess_MessMedia", messageMedia);

        bc.SendMess.ShowNumUsers();
    },

    OpenLocalStorage: function () {
        var listUsers,
            messageText,
            messageMedia;

        listUsers = localStorage.getItem("bcSendMess_ListUsers");
        messageText = localStorage.getItem("bcSendMess_MessText");
        messageMedia = localStorage.getItem("bcSendMess_MessMedia");

        if (listUsers) {
            bc.SendMess.ListUsers = listUsers;
            document.getElementById("bcSendMess_ListUsers").value = listUsers;
        }
        if (messageText) {
            bc.SendMess.MessageText = messageText;
            document.getElementById("bcSendMess_MessText").value = messageText;
        }
        if (messageMedia) {
            bc.SendMess.MessageMedia = messageMedia;
            document.getElementById("bcSendMess_MessMedia").value = messageMedia;
        }

        bc.SendMess.ShowNumUsers();
    },

    ClearAll: function () {

        localStorage.removeItem("bcSendMess_ListUsers");
        localStorage.removeItem("bcSendMess_MessText");
        localStorage.removeItem("bcSendMess_MessMedia");

        document.getElementById("bcSendMess_ListUsers").value = "";
        document.getElementById("bcSendMess_MessText").value = "";
        document.getElementById("bcSendMess_MessMedia").value = "";

        bc.SendMess.ListUsers = null;
        bc.SendMess.MessageText = null;
        bc.SendMess.MessageMedia = null;

        bc.SendMess.Users = null;
        bc.SendMess.Massage = null;
        bc.SendMess.Media = null;

        bc.SendMess.ShowNumUsers();
    },

    /////////////------------------------------------
    AddSpaseEnd: function (n) {
        if (n > 99999999) return "";
        if (n > 9999999) return " ";
        if (n > 999999) return "  ";
        if (n > 99999) return "   ";
        if (n > 9999) return "    ";
        if (n > 999) return "     ";
        if (n > 99) return "      ";
        if (n > 9) return "       ";
        return "";
    },

    UserGetAsLine: function (id, firstName, lastName) {
        return id.toString() + bc.SendMess.AddSpaseEnd(id) + " \t- " + firstName + " " + lastName;
    },


    getRandomInt: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }, //  Случайное целое между min и max

    ParseRegulExpressionMessage: function (regString) {
        //var simple = " Lala   { -   --+ | --   +- | -+--     |     +--- } text { #   |  #  |   # } ---***-- ";

        if (!regString || typeof (regString) !== "string") {
            return null;
        }

        var arrStrDirty,
            result,
            i,
            j;

        arrStrDirty = regString.match(/[^{}]*/g);

        result = [];

        for (i = 0; i < arrStrDirty.length; i++) {
            if (arrStrDirty[i] === "") continue;

            if (arrStrDirty[i].indexOf('|') !== -1) {
                arrStrDirty[i] = arrStrDirty[i].match(/[^|]*/g);

                result.push(new Array());

                for (j = 0; j < arrStrDirty[i].length; j++) {
                    if (arrStrDirty[i][j] === "") continue;

                    result[result.length - 1].push(arrStrDirty[i][j]);
                }
            } else {
                result.push(arrStrDirty[i]);
            }
        }
        return result;
    }, //Разбор регулярного выражения Message (return obj)

    ParseRegulExpressionMedia: function (regString) {

        if (!regString || typeof (regString) !== "string") return null;

        var result = regString.match(/video[\-]?\d+_\d+|photo[\-]?\d+_\d+/g);

        for (var i = 0; i < result.length; i++) {
            var tmp = result[i];
            result[i] = {
                type: tmp.match(/video|photo/)[0],
                id: tmp.match(/[\-]?\d+_\d+/)[0]
            };
        }
        return result;
    }, //Разбор регулярного выражения Message (return obj)

    CreateString: function (arrString) {

        var result = "";

        if (!arrString) return result;

        for (var i = 0; i < arrString.length; i++) {
            if (typeof (arrString[i]) !== "string") {
                result += arrString[i][bc.SendMess.getRandomInt(0, arrString[i].length - 1)];
            } else {
                result += arrString[i];
            }
        }

        return result;
    }, // Создание случайной строки из массива 


    getUsers: function () {

        bc.SendMess.Users = null;
        if (!bc.SendMess.ListUsers || typeof (bc.SendMess.ListUsers) !== "string") return;

        var arrlistUsers = bc.SendMess.ListUsers.split(/\n/g);

        var numUsers = arrlistUsers.length;

        bc.SendMess.Users = new Array();

        for (var i = 0; i < numUsers; i++) {
            var nameObj = arrlistUsers[i].match(/\-\s[\w\W]+/);
            if (!nameObj) continue;
            var name = nameObj[0];
            var arrName = name.split(/\s+/g);
            bc.SendMess.Users.push({
                id: arrlistUsers[i].match(/\d+/)[0],
                firstName: arrName[1],
                lastName: arrName[2]
            });
        }
    },
    getListUser: function () {
        bc.SendMess.ListUsers = "";
        if (!bc.SendMess.Users) return;
        for (var i = 0; i < bc.SendMess.Users.length; i++) {
            bc.SendMess.ListUsers += bc.SendMess.UserGetAsLine(bc.SendMess.Users[i].id, bc.SendMess.Users[i].firstName, bc.SendMess.Users[i].lastName) + "\n";
        }
    },

    getMessage: function () {

        bc.SendMess.Message = null;

        if (!bc.SendMess.MessageText || typeof (bc.SendMess.MessageText) !== "string") return;

        bc.SendMess.Message = bc.SendMess.ParseRegulExpressionMessage(bc.SendMess.MessageText);
    },

    getMessageRand: function () {
        return bc.SendMess.CreateString(bc.SendMess.Message);
    },

    getMedia: function () {
        bc.SendMess.Media = null;
        if (!bc.SendMess.MessageMedia || typeof (bc.SendMess.MessageMedia) !== "string") return;
        bc.SendMess.Media = bc.SendMess.ParseRegulExpressionMedia(bc.SendMess.MessageMedia);
    },

    /// send

    Send: function () {

        if (bc.SendMess.isSendStep === 0) { // Создаем новые обекты 

            if (location.pathname !== "/im") {
                jQuery("#l_msg a")[0].click();
                bc.SendMess.timer.Send = window.setTimeout(bc.SendMess.Send, 2500);
                return;
            }

            bc.SendMess.isSendStep = 1;

            bc.SendMess.SeveLocalStorage();
            bc.SendMess.getUsers();
            bc.SendMess.getMessage();
            bc.SendMess.getMedia();
        }
        if (bc.SendMess.Users === null || bc.SendMess.Users.length === 0) {
            bc.SendMess.Stop();
            return;
        }
        var kInterval = bc.Lib.GetInterval("bcSendMess_IntervalSend", 4, 6) / 7.0;
        //console.log("kInterval = " + kInterval);
        if (bc.SendMess.Users.length === 0) {

            bc.SendMess.Stop();
        } // проверка на коней списка пользователей

        bc.SendMess.timer.SelectDialog = window.setTimeout(bc.SendMess.SelectDialog, 100 * kInterval);

        bc.SendMess.timer.WriteMessage = window.setTimeout(bc.SendMess.WriteMessage, 1000 * kInterval);

        bc.SendMess.timer.AddMedia = window.setTimeout(bc.SendMess.AddMedia, 1500 * kInterval);

        bc.SendMess.timer.SendMessage = window.setTimeout(bc.SendMess.SendMessage, 2000 * kInterval);

        bc.SendMess.timer.CloseDialog = window.setTimeout(bc.SendMess.CloseDialog, 5000 * kInterval);

        bc.SendMess.timer.RemoveUsers = window.setTimeout(bc.SendMess.RemoveUsers, 6000 * kInterval);
        bc.SendMess.timer.UpDateListUser = window.setTimeout(bc.SendMess.UpDateListUser, 6500 * kInterval);

        bc.SendMess.timer.Send = window.setTimeout(bc.SendMess.Send, 7000 * kInterval);


    },

    Stop: function () {
        bc.SendMess.isSendStep = 0;

        window.clearTimeout(bc.SendMess.timer.Send);
        window.clearTimeout(bc.SendMess.timer.RemoveUsers);
        window.clearTimeout(bc.SendMess.timer.UpDateListUser);
        window.clearTimeout(bc.SendMess.timer.SelectDialog);
        window.clearTimeout(bc.SendMess.timer.CloseDialog);
        window.clearTimeout(bc.SendMess.timer.WriteMessage);
        window.clearTimeout(bc.SendMess.timer.AddMedia);
        window.clearTimeout(bc.SendMess.timer.SendMessage);

        var e = document.getElementById("bcSendMess_Stop");
        if (e) e.click();
    },

    SelectDialog: function () {
        if (bc.SendMess.isSendStep === 0) return;
        jQuery(jQuery(".nim-dialog")[0]).attr("data-peer", bc.SendMess.Users[0].id).click();
    },

    WriteMessage: function () {
        if (bc.SendMess.isSendStep === 0) return;



        //if (bc.SendMess.isSendStep === 1) {
        //    ///For Add Media
        //    var eAddMedia = document.getElementById("im_add_media_link");
        //    if (eAddMedia) eAddMedia.click();

        //    var mediaMenu = document.getElementsByClassName("add_media_menu");
        //    if (mediaMenu !== null)
        //        for (var i = 0; i < mediaMenu.length; i++)
        //            mediaMenu[i].style.display = "none";

        //    bc.SendMess.isSendStep = 2;
        //}

        var fieldInput = jQuery(".im_editable:visible")[0];

        fieldInput.innerHTML = bc.SendMess.getMessageRand().replace(/\n/g, "<br>");
        fieldInput.click();
    },

    AddMedia: function () {
        if (bc.SendMess.isSendStep === 0) return;

        if (!bc.SendMess.Media) return;
        for (var i = 0; i < bc.SendMess.Media.length && i < 10; i++) {

            if (bc.SendMess.Media[i].type === "video") {
                try {
                    cur.chooseVideoMedia(null, bc.SendMess.Media[i].id);
                } catch (e) {
                    bc.Log.Add(e + "Попробуйте вручную добавить файл видео");

                }
            }
            if (bc.SendMess.Media[i].type === "photo") {
                try {
                    cur.choosePhotoMulti(bc.SendMess.Media[i].id, cur.chooseMedia.pbind('photo', bc.SendMess.Media[i].id, " "));
                    
                } catch (e) {
                    bc.Log.Add(e + "Попробуйте вручную добавить файл фото");
                    bc.get("bcSendMess_Stop").click();
                }
            }

            //
        }
    },

    SendMessage: function () {
        if (bc.SendMess.isSendStep === 0) return;
        jQuery("._im_send")[0].click();

    },

    CloseDialog: function () {
        if (bc.SendMess.isSendStep === 0) return;

        var captcha = document.getElementsByClassName("captcha");
        if (captcha.length > 0) { // страно но она при отсутствии равна []
            console.log("captcha - SendMessage");
            setTimeout(bc.SendMess.CloseDialog, 5000);

            if (bc.ge("bcSendMess_isSound").checked) {
                bc.Lib.audio.volume = bc.Lib.audioVolume;
                bc.Lib.audio.play();
            }

            return;
        }


        // Проверка на ошибку

        if (jQuery("ul.im-mess-stack--mess li._im_mess_faild").length > 0) {
            bc.Log.Add("Не удалось отправить пользователю: " +
                '<a href="/id' + bc.SendMess.Users[0].id + '" target="_blank">'
                + bc.SendMess.Users[0].firstName + " " + bc.SendMess.Users[0].lastName + "</a>");
        }

        jQuery(jQuery(".ui_rmenu_item_sel")[0]).find("button.im-right-menu--close")[0].click();
    },

    RemoveUsers: function () {
        if (bc.SendMess.isSendStep === 0) return; // проверка на останоное задание

        bc.SendMess.Users.shift(); //удаляем первого пользователя

    },

    UpDateListUser: function () {
        bc.SendMess.getListUser();
        if (bc.SendMess.ListUsers !== null) {
            document.getElementById("bcSendMess_ListUsers").value = bc.SendMess.ListUsers;
        } else {
            document.getElementById("bcSendMess_ListUsers").value = "";
        }
        bc.SendMess.ShowNumUsers();
    }
}; // bc.SendMess

bc.SendMess.Empty = function () { };
console.log("Load Send");