﻿"use strict";

var bc = {
    host: "http://localhost:8080/Bot/",
    hostApi: "http://xn----htbbbbz1abrejgc7d8c.com/nromik_BotCat/Api/",
    //host: "http://xn----htbbbbz1abrejgc7d8c.com/nromik_BotCat/",
    isLog: true,
    ge: function(id) {
        return document.getElementById(id.toString());
    },
    vkUser: "",
    blockInvate: [],
    session: Math.floor(Math.random()*2147483647)
};
console.log("bc.host = " + bc.host);

bc.View = {
    countLoadingScript: 0, // для отслеживания загрузки всех скриптов
    countLoadedScript: 0, // для отслеживания загрузки всех скриптов

    AddLink: function(href, id) {
        if (bc.ge(id)) {
            document.head.removeChild(bc.ge(id));
        }
        var link = document.createElement("link");
        link.rel = "stylesheet";
        link.type = "text/css";
        link.href = href;
        if (id) link.id = id;
        document.head.appendChild(link);
    },
    AddScript: function(src, id) {
        if (bc.ge(id)) {
            document.head.removeChild(bc.ge(id));
        }
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = src;
        if (id) script.id = id;
        document.head.appendChild(script);

        script.onload = function() {
            bc.View.countLoadedScript++;
            bc.View.Init();
        }
        bc.View.countLoadingScript++;

        script.onerror = function() {
            alert( "Ошибка загрузки: " + this.src );
        };


    },

    /// Загрузка формы 
    Show: function () {

        bc.View.AddScript(bc.host + "Lib.js", "bc.Lib.js"); // no window.$
        bc.View.AddScript(bc.host + "Log.js", "bc.Log.js"); // no window.$
        bc.View.AddScript(bc.host + "Send.js", "bc.Send.js"); // no window.$
        bc.View.AddScript(bc.host + "Invite.js", "bc.Invite.js"); // no window.$

        bc.View.AddScript(bc.host + "jquery-3.1.0-my.js", "bc.jquery-3.1.0-my.js"); // no window.$
        bc.View.AddLink("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css", "is_style_fa");
        bc.View.AddLink(bc.host + "bcStyle.css", "bc.bcStyle.css");

        

        //bc.View.NoConflictjQuesy();

        if (bc.ge("BotCat")) {
            document.body.removeChild(bc.ge("BotCat"));
        }

        var div = document.createElement("div");
        div.id = "BotCat";
        div.innerHTML = bc.View.html;
        document.body.appendChild(div);


        // удаляем панельку с лева стоб случайно не перейти на другую станичку
        bc.ge("stl_left") && document.body.removeChild(bc.ge("stl_left"));

        //setTimeout(bc.View.Init, 100);

    },

    Init: function () {

        if (bc.View.countLoadedScript !== bc.View.countLoadingScript) return;

        bc.View.InitMenu();
        bc.View.InitButtonControl();
        bc.View.InitButtonSendMess();
        bc.Log.ClearAll();
        bc.vkUser = bc.ge("l_pr").children[0].href.split(/\//)[3];
    },

    InitMenu: function() {
        bc.ge("bcDropMenu").onclick = bc.View.ShowMenu;

        var menu = bc.ge("bcMenu-dropdown");
        for (var i = 0; i < menu.children.length; i++) {
            menu.children[i].addEventListener("click", bc.View.LiClick);
        }
    }, //
    ShowMenu: function() {
        var e = bc.ge("bcMenu-dropdown");

        if (e.style.display === "none") {
            e.style.display = "block";
        } else {
            e.style.display = "none";
        }

    },
    LiClick: function(e) {
        var menu = bc.ge("bcMenu-dropdown");
        var item = null;
        for (var i = 0; i < menu.children.length; i++) {
            menu.children[i].className = "";
            if (menu.children[i].dataset.section) {
                item = bc.ge(menu.children[i].dataset.section);
                if (item)
                    item.style.display = "none";
            }
        }
        e.target.className = "bcActive";
        item = bc.ge(e.target.dataset.section);
        if (item) item.style.display = "block";

        bc.ge("bcMenuCurront").innerText = e.target.innerText;

        bc.ge("bcMenu-dropdown").style.display = "none";
    },

    InitButtonControl: function() {
        var bottonsControl = document.getElementsByClassName("bcButtonControl");
        if (bottonsControl) {
            for (var i = 0; i < bottonsControl.length; i++) {
                bottonsControl[i].addEventListener("click", bc.View.ButtonControlClick, true);
            }
        }
    },

    ButtonControlClick: function(e) {
        var parent = e.target.parentElement;
        if (parent.nodeName !== "SPAN") parent = parent.parentElement;
        for (var i = 0; i < parent.children.length; i++) {
            parent.children[i].className = "bcButtonControl";
        }
        if (e.target.nodeName === "BUTTON") {
            e.target.className += " bcButtonControlActive";
        } else {
            e.target.parentElement.className += " bcButtonControlActive";
        }
    },

    InitButtonSendMess: function() {
        bc.ge("bcSendMess_ListUsers_Clean").addEventListener("click", bc.View.ClearArea, false);
        bc.ge("bcSendMess_MessText_Clean").addEventListener("click", bc.View.ClearArea, false);
        bc.ge("bcSendMess_MessMedia_Clean").addEventListener("click", bc.View.ClearArea, false);

        bc.ge("bcInvite_ListUsers_Clean").addEventListener("click", bc.View.ClearArea, false);


        //sound
        bc.ge("bcSendMess_isSound").addEventListener("change", bc.Lib.TestAudio, false);
        bc.ge("bcInvite_isSound").addEventListener("change", bc.Lib.TestAudio, false);

        bc.ge("bcHeader").addEventListener("mousedown", bc.View.DragPanel);
        bc.ge("bcHeader").addEventListener("dblclick", bc.View.DisplayBody);
    },


    DragPanel: function(event) {
        // елемент который перемещяем 
        var eHeader = bc.ge("bcPanel");

        // координаты мыши в начале перетаскивания.
        var startX = event.clientX,
            startY = event.clientY;

        // начальные координаты элемента, который будет перемещаться.
        var origX = eHeader.offsetLeft,
            origY = eHeader.offsetTop;
        

        var deltaX = event.clientX - eHeader.offsetLeft,
            deltaY = event.clientY - eHeader.offsetTop;

        document.addEventListener("mousemove", moveHandler, true);
        document.addEventListener("mouseup", upHandler, true);

        function moveHandler(e) {
            var x = (e.clientX - deltaX),
                y = (e.clientY - deltaY);

            var panel = bc.ge("bcPanel");

            var maxX = window.parent.innerWidth - panel.clientWidth;
            var maxY = window.parent.innerHeight - panel.clientHeight;

            if (0 < x && x < maxX)
                bc.ge("bcPanel").style.left = x + "px";
            if (0 < y && y < maxY)
                bc.ge("bcPanel").style.top = y + "px";
        }

        function upHandler (e) {
            document.removeEventListener("mousemove", moveHandler, true);
            document.removeEventListener("mouseup", upHandler, true);
        }

    },

    DisplayBody: function() {
        var displayBody = bc.ge("bcPanel").dataset.display;
        if (displayBody === "hide") {
            bc.ge("bcMenu").style.display = "block";
            bc.ge("bcSection").style.display = "block";
            bc.ge("bcFooter").style.display = "block";
            bc.ge("bcPanel").dataset.display = "show";
        } else {
            bc.ge("bcMenu").style.display = "none";
            bc.ge("bcSection").style.display = "none";
            bc.ge("bcFooter").style.display = "none";
            bc.ge("bcPanel").dataset.display = "hide";
        }

    },

    ClearArea: function(e) {

        var areaText = e.target.previousElementSibling;
        if (areaText)
            areaText.value = "";
    }


}; //bc.View

bc.View.html = '<div id=bcPanel><div id=bcHeader></div><div id=bcMenu class=dropdown><div id=bcDropMenu>Меню <i class="fa fa-angle-double-right"></i><span id=bcMenuCurront>Парсинг списка друзей</span></div><ul id=bcMenu-dropdown style="display: none;"><li class=bcActive data-section=bcSendMess>Рассылка сообщений</li><li data-section=bcInvite>Приглашение на встречу</li><hr><li data-section=bcAbout>О програме</li></ul></div><div id=bcSection><div id=bcSendMess><button id=bcSendMess_New onclick=bc.SendMess.NewParseeUser()>New </button><button id=bcSendMess_Open onclick=bc.SendMess.OpenLocalStorage()>Open</button><button id=bcSendMess_Save onclick=bc.SendMess.SeveLocalStorage()>Save</button><button id=bcSendMess_Clear onclick=bc.SendMess.ClearAll()>Clear</button><p>Cписок рассылки: = <span id=bcSendMess.NumUsers>0</span><textarea id=bcSendMess_ListUsers></textarea><i class="fa fa-times" id=bcSendMess_ListUsers_Clean></i></p><hr style="margin: 1px 0;"><p>Сообщение:<textarea id=bcSendMess_MessText></textarea><i class="fa fa-times" id=bcSendMess_MessText_Clean></i></p><p>Медиа файлы:<textarea id=bcSendMess_MessMedia></textarea><i class="fa fa-times" id=bcSendMess_MessMedia_Clean></i></p><p><span>Interval:</span> <input type=text id=bcSendMess_IntervalSend value="5 - 10"><span><button onclick=bc.SendMess.Send() class=bcButtonControl><i class="fa fa-paper-plane-o"></i></button><button onclick=bc.SendMess.Stop() id=bcSendMess_Stop class="bcButtonControl bcButtonControlActive"><i class="fa fa-stop"></i></button></span><br><input type=checkbox checked=checked id=bcSendMess_isSound> включить звук</p></div><div id=bcInvite style="display: none;"><p id=begin>Начать приглашение пользователей: <br><input type=radio name=bcInvate_Begin value=0 onchange=bc.Invite.BeginView(0) checked=checked> Текущяя позиция <br><input type=radio name=bcInvate_Begin value=1 onchange=bc.Invite.BeginView(1)> Начало списка<br> <input type=radio name=bcInvate_Begin value=2 onchange=bc.Invite.BeginView(2)> C номера <input type=number id=bcInvite_StartWithNum placeholder=NaN><br><input type=radio name=bcInvate_Begin value=3 onchange=bc.Invite.BeginView(3)> Последнего приглашенного<br> <input type=radio name=bcInvate_Begin value=4 onchange=bc.Invite.BeginView(4)> Найти:<input type=text id=bcInvite_StartWithUser placeholder=http://vk.com/user></p><hr style="margin: 1px 0;"><p><span>Interval:</span> <input type=text id=bcInvite_Interval value="2 - 3"><span><button title=Search onclick=bc.Invite.Search() class=bcButtonControl><i class="fa fa-search"></i></button><button title=Start onclick=bc.Invite.Start() class=bcButtonControl><i class="fa fa-play"></i></button><button title=Stop onclick=bc.Invite.Stop() id=bcInvite_Stop class="bcButtonControl bcButtonControlActive"><i class="fa fa-stop"></i></button></span><br><input type=checkbox checked=checked id=bcInvite_isSound> включить звук <br><input type=checkbox onchange=bc.Invite.LoadBlockList()> Block List = <span id=bcInvite_countBlockUser>0</span></p><hr style="margin: 1px 0;"><p>Приглашено: <span id=bcInvite_invitedUsers></span><br> Выслано приглашений: <span id=bcInvite_sendInvitedUsers></span><br> Текущий пользватель:<span id=bcInvite_currentUser></span><br><progress id=bcInviteProgress value=0 max=100></progress></p><p>Не удалось пригласить: = <span id=bcInvite_NumUsers>0</span><textarea id=bcInvite_ListUsers></textarea><i class="fa fa-times" id=bcInvite_ListUsers_Clean></i></p></div></div><div id=bcFooter><p>Лог:<i class="fa fa-times" onclick=bc.Log.ClearAll()></i></p><div id=bcFooter_ErrorList><ol id=bcFooter_ErrorList_ol reversed><li>Some Error<i class="fa fa-times"></i></li></ol></div></div></div>';

bc.View.Empty = function() {};
bc.View.Show();
