﻿"use strict";

bc.Lib = {
    View: {},
    audio: new Audio("http://www.w3schools.com/jsref/horse.ogg"),
    audioVolume: 0.5,

    TestAudio: function(e) {
        if (e.target.checked) {
            bc.Lib.audio.volume = bc.Lib.audioVolume;
            bc.Lib.audio.play();
        }
    },

    GetInterval: function(id, min, max) {

        var eInterval = jQuery("#" + id)[0];
        var t = eInterval.value.match(/\d+/g);

        var t0 = min || 5, t1 = max || 7; // 
        if (t) {
            if (t[0]) {
                t0 = t[0];

                if (t[1]) t1 = t[1];
                else t1 = t0;
            }
        }
        var rand = bc.Lib.GetRandomInt(t0, t1);

        //console.log("bc.Lib.View.GetInterval --- t1 = " + t0 + " \tt2 = " + t1 + " \trand = " + rand);
        return rand;

    },

    GetRandomInt: function(min, max) {
        min = parseInt(min);
        max = parseInt(max);
        return Math.floor(Math.random() * (max * 1 - min + 1)) + min;
    },

    Translit: function(text) {
        // Символ, на который будут заменяться все спецсимволы
        var space = '_';
        // Берем значение из нужного поля и переводим в нижний регистр
        //var text = $('#name').val().toLowerCase();

        // Массив для транслитерации
        var transl = {
            'А': 'A',
            'Б': 'B',
            'В': 'V',
            'Г': 'G',
            'Д': 'D',
            'Е': 'E',
            'Ё': 'E',
            'Ж': 'Zh',
            'З': 'Z',
            'И': 'I',
            'Й': 'J',
            'К': 'K',
            'Л': 'L',
            'М': 'M',
            'Н': 'N',
            'О': 'O',
            'П': 'P',
            'Р': 'R',
            'С': 'S',
            'Т': 'T',
            'У': 'U',
            'Ф': 'F',
            'Х': 'H',
            'Ц': 'C',
            'Ч': 'Ch',
            'Ш': 'Sh',
            'Щ': 'Sh',
            'Ъ': '\'',
            'Ы': 'Y',
            'Ь': '\'',
            'Э': 'E',
            'Ю': 'Yu',
            'Я': 'Ya',

            'а': 'a',
            'б': 'b',
            'в': 'v',
            'г': 'g',
            'д': 'd',
            'е': 'e',
            'ё': 'e',
            'ж': 'zh',
            'з': 'z',
            'и': 'i',
            'й': 'j',
            'к': 'k',
            'л': 'l',
            'м': 'm',
            'н': 'n',
            'о': 'o',
            'п': 'p',
            'р': 'r',
            'с': 's',
            'т': 't',
            'у': 'u',
            'ф': 'f',
            'х': 'h',
            'ц': 'c',
            'ч': 'ch',
            'ш': 'sh',
            'щ': 'sh',
            'ъ': '\'',
            'ы': 'y',
            'ь': '\'',
            'э': 'e',
            'ю': 'yu',
            'я': 'ya',

            ' ': ' ',
            '_': space,
            '`': space,
            '~': space,
            '!': space,
            '@': space,
            '#': space,
            '$': space,
            '%': space,
            '^': space,
            '&': space,
            '*': space,
            '(': space,
            ')': space,
            '-': space,
            '\=': space,
            '+': space,
            '[': space,
            ']': space,
            '\\': space,
            '|': space,
            '/': space,
            '.': space,
            ',': space,
            '{': space,
            '}': space,
            '\'': space,
            '"': space,
            ';': space,
            ':': space,
            '?': space,
            '<': space,
            '>': space,
            '№': space
        }

        var result = "";
        var curentSim = "";

        for (var i = 0; i < text.length; i++) {
            // Если символ найден в массиве то меняем его
            if (transl[text[i]] !== undefined) {
                if (curentSim !== transl[text[i]] || curentSim !== space) {
                    result += transl[text[i]];
                    curentSim = transl[text[i]];
                }
            }
            // Если нет, то оставляем так как есть
            else {
                result += text[i];
                curentSim = text[i];
            }
        }

        result = result.trim();

        // Выводим результат 
        return result;
    }
};

bc.Lib.Empty = function () { };
console.log("Load Lib");